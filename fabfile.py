from fabric import task


@task
def sh(c, command):
    print(c)
    c.run(command)

@task
def update(c):
    print(c)
    c.run('yadm pull && (cd ~/.fzf && git pull) && vim +PlugUpdate +qall && vim +PlugUpgrade +qall')
    c.run('zsh <<< "export TERM=screen-256color; source $HOME/.zshrc; zplug update"')
