hs.loadSpoon("SpoonInstall")
spoon.use_syncinstall = true
spoon.SpoonInstall:andUse("ReloadConfiguration")
spoon.SpoonInstall.repos.ShiftIt = {
   url = "https://github.com/peterklijn/hammerspoon-shiftit",
   desc = "ShiftIt spoon repository",
   branch = "master",
}
spoon.SpoonInstall:andUse("ShiftIt", { repo = "ShiftIt" })

spoon.ReloadConfiguration:start()

spoon.ShiftIt:bindHotkeys({
  left = {{ 'ctrl', 'cmd' }, 'h' },
  right = {{ 'ctrl', 'cmd' }, 'l' },
  up = {{ 'ctrl', 'cmd' }, 'k' },
  down = {{ 'ctrl', 'cmd' }, 'j' },
  upleft = {{ 'ctrl', 'alt', 'cmd' }, 'h' },
  upright = {{ 'ctrl', 'alt', 'cmd' }, 'l' },
  botleft = {{ 'ctrl', 'alt', 'cmd' }, 'j' },
  botright = {{ 'ctrl', 'alt', 'cmd' }, 'k' },
  maximum = {{ 'ctrl', 'alt', 'cmd' }, 'space' }
});

hs.hotkey.bind({'alt', 'ctrl', 'cmd'}, 'p', function()
  local win = hs.window.focusedWindow()
  local screen = win:screen()
  win:move(win:frame():toUnitRect(screen:frame()), screen:next(), true, 0)
end)
