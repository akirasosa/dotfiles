source ~/.zplug/init.zsh
zplug "plugins/autojump", from:oh-my-zsh
zplug "plugins/common-aliases", from:oh-my-zsh
zplug "plugins/vi-mode", from:oh-my-zsh
zplug "mafredri/zsh-async"
zplug "esc/conda-zsh-completion"
zplug "zsh-users/zsh-completions"
zplug "zsh-users/zsh-syntax-highlighting", defer:2
zplug chabou/pure-now, use:pure.zsh, from:github, as:theme
zplug 'zplug/zplug', hook-build:'zplug --self-manage'
if ! zplug check; then
   zplug install
fi
zplug load

#zstyle ':completion:*' menu select
zstyle ':completion:*' menu select
zstyle ':completion:*' matcher-list '' \
'm:{a-z\-}={A-Z\_}' \
'r:[^[:alpha:]]||[[:alpha:]]=** r:|=* m:{a-z\-}={A-Z\_}' \
'r:|?=** m:{a-z\-}={A-Z\_}'

bindkey -v

HISTFILE=~/.zsh_history
HISTSIZE=1000000
SAVEHIST=1000000

autoload -U is-at-least
autoload history-search-end
export CLICOLOR=1
source ~/.ls_colors.zsh

export FZF_DEFAULT_OPTS="--extended --cycle --reverse --ansi"

setopt inc_append_history
setopt share_history
setopt hist_ignore_all_dups
setopt hist_ignore_dups
setopt hist_ignore_space
unsetopt hist_verify
setopt hist_reduce_blanks
setopt hist_save_no_dups
setopt hist_no_store
setopt hist_expand
setopt nocorrectall

zle -N history-beginning-search-backward-end history-search-end
zle -N history-beginning-search-forward-end history-search-end
bindkey "^P" history-beginning-search-backward-end
bindkey "^N" history-beginning-search-forward-end

bindkey '^F' fzf-file-widget

fzf-autojump-widget() {
  local selected_dir=$(autojump -s | perl -e 'print reverse <>' | sed -e '1,7d' | awk '{print $2}' | fzf -q "$LBUFFER")
  if [ -n "$selected_dir" ]; then
    BUFFER="cd ${selected_dir}"
    zle accept-line
  fi
  zle clear-screen
}
zle -N fzf-autojump-widget
bindkey '^J' fzf-autojump-widget

fzf-ghq-list-widget() {
  local selected_dir=$(ghq list --full-path | fzf -q "$LBUFFER")
  if [ -n "$selected_dir" ]; then
    BUFFER="cd ${selected_dir}"
    zle accept-line
  fi
  zle clear-screen
}
zle -N fzf-ghq-list-widget
bindkey '^]' fzf-ghq-list-widget

eval "$(direnv hook zsh)"

#export PATH="$HOME/.plenv/bin:$PATH"
#eval "$(plenv init -)"

[ -f ~/.fzf.zsh ] && source ~/.fzf.zsh

autoload -U +X bashcompinit && bashcompinit
complete -o nospace -C /opt/homebrew/bin/terraform terraform
