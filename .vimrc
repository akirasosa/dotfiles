" Common
set nocompatible
set noswapfile
filetype off
filetype plugin on

" Plugin
if empty(glob('~/.vim/autoload/plug.vim'))
  silent !curl -fLo ~/.vim/autoload/plug.vim --create-dirs
        \ https://raw.githubusercontent.com/junegunn/vim-plug/master/plug.vim
  autocmd VimEnter * PlugInstall --sync | source $MYVIMRC
endif

call plug#begin('~/.vim/plugged')
Plug 'scrooloose/nerdtree', { 'on': 'NERDTreeToggle' }
Plug 'scrooloose/syntastic'
Plug 'tpope/vim-repeat'
Plug 'tpope/vim-surround'
Plug 'w0ng/vim-hybrid'
Plug 'kien/ctrlp.vim'
Plug 'scrooloose/nerdcommenter'
Plug 'dag/vim-fish'
call plug#end()

" OS dependent font setting
if has("win32")
  set guifont=MS_Gothic:h11:cSHIFTJIS
  set printfont=MS_Gothic:h9:cSHIFTJIS
  set printoptions=number:y
elseif has('mac')
  set guifont=Monaco:h12
  set linespace=4
else
  set guifontset=a14,r14,k14
endif

" Visual
set t_Co=256
set background=dark
colorscheme hybrid
set guioptions=
set hlsearch     " do not highlight searched for phrases
set incsearch    " BUT do highlight as you type you search phrase
hi Search ctermbg=Blue
hi Search ctermfg=White
set number
set wildmenu
set ruler

" Keymap
imap <C-j> <esc>
set pastetoggle=<C-E>
let mapleader = "\<Space>"
nnoremap <C-j><C-j> :nohlsearch<CR>
xnoremap p pgvy

" Indent
set fo=tcrqn     " See Help (complex)
set autoindent   " autoindent
set smartindent  " smartindent
set cindent      " do c-style indenting
set smarttab     " use tabs at the start of a line, spaces elsewhere
set expandtab
set tabstop=2
set shiftwidth=2
set softtabstop=0

" NERDTree
nnoremap <silent> tt :NERDTreeToggle<CR>

" ctrlp
let g:ctrlp_user_command = ['.git', 'cd %s && git ls-files -co --exclude-standard']

" NERDCommenter
map <Leader>x <space>c<space>

" show border on 80 column
if exists('+colorcolumn')
  set colorcolumn=80
else
  au BufWinEnter * let w:m2=matchadd('ErrorMsg', '\%>80v.\+', -1)
endif

let &t_SI = "\e[6 q"
let &t_EI = "\e[2 q"
