if status is-interactive
	switch (uname)
    case Linux
    case Darwin
		 eval (/opt/homebrew/bin/brew shellenv)
    case FreeBSD NetBSD DragonFly
    case '*'
	end
end

function __fzf_z -d 'Find and Jump to a recent directory.'
  set -l query (commandline)

  if test -n $query
    set flags --query "$query"
  end

  z -l | awk '{ print $2 }' | eval (__fzfcmd) "$FZF_DEFAULT_OPTS $flags" | read recent
  if [ $recent ]
      cd $recent
      commandline -r ''
      commandline -f repaint
  end
end


fish_vi_key_bindings
for mode in insert default visual
    bind -M $mode \cf forward-char
    #bind -M $mode \cp up-or-search
    #bind -M $mode \cn down-or-search
    bind -M $mode \cp history-prefix-search-backward
    bind -M $mode \cn history-prefix-search-forward
    bind -M $mode \cj __fzf_z
end

source "$HOME/.rye/env.fish"
