# For perl
export LC_CTYPE=en_US.UTF-8
export LC_ALL=en_US.UTF-8

export PATH="/usr/local/bin:$PATH"
export PATH="/usr/local/sbin:$PATH"
export PATH=$HOME/bin:$PATH

# Node
export PATH=$HOME/.nodebrew/current/bin:$PATH

# Go
export PATH=$HOME/go/bin:$PATH

# Rust
source "$HOME/.cargo/env"
export PATH=$HOME/.cargo/bin:$PATH

export PKG_CONFIG_PATH=/usr/local/lib/pkgconfig/

# CUDA
export PATH=$PATH:/usr/local/cuda/bin
export LD_LIBRARY_PATH="/usr/local/cuda/lib64:$LD_LIBRARY_PATH"

# MySQL
export PATH="/usr/local/opt/mysql-client/bin:$PATH"

# Google Cloud SDK.
export GOOGLE_CLOUD_SDK_HOME=$HOME/local/google-cloud-sdk
if [ -f "$GOOGLE_CLOUD_SDK_HOME/path.zsh.inc" ]; then . "$GOOGLE_CLOUD_SDK_HOME/path.zsh.inc"; fi
if [ -f "$GOOGLE_CLOUD_SDK_HOME/completion.zsh.inc" ]; then . "$GOOGLE_CLOUD_SDK_HOME/completion.zsh.inc"; fi
export PATH=$GOOGLE_CLOUD_SDK_HOME//bin:$PATH

# Anaconda3
export CONDA_HOME=$HOME/local/anaconda3
__conda_setup="$("$CONDA_HOME/bin/conda" 'shell.bash' 'hook' 2> /dev/null)"
if [ $? -eq 0 ]; then
    eval "$__conda_setup"
else
    if [ -f "$CONDA_HOME/etc/profile.d/conda.sh" ]; then
        . "$CONDA_HOME/etc/profile.d/conda.sh"
    fi
fi
unset __conda_setup
export PATH="$CONDA_HOME/bin:$PATH"

# LibTorch
export LIBTORCH_HOME=$HOME/local/libtorch

export MECABRC='/etc/mecabrc'

# Android
export ANDROID_HOME=$HOME/Library/Android/sdk
export PATH=$PATH:$ANDROID_HOME/emulator
export PATH=$PATH:$ANDROID_HOME/platform-tools
export PATH=$PATH:$ANDROID_HOME/tools
export PATH=$PATH:$ANDROID_HOME/tools/bin

# NVM
export NVM_DIR="$HOME/.nvm"
[ -s "$NVM_DIR/nvm.sh" ] && \. "$NVM_DIR/nvm.sh"  # This loads nvm
[ -s "$NVM_DIR/bash_completion" ] && \. "$NVM_DIR/bash_completion"  # This loads nvm bash_completion

# Java
#export JAVA_HOME=$(/usr/libexec/java_home)
export DISPLAY=localhost:0.0
